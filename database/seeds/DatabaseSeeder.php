<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        factory(User::class)->create(
            [
                'email' => 'utpalseu@gmail.com',
                'name' => 'Utpal'
            ]
        );
        factory(User::class)->create(
            [
                'email' => 'rajesh@gmail.com',
                'name' => 'Rajesh'
            ]
        );
        factory(User::class)->create(
            [
                'email' => 'rana@gmail.com',
                'name' => 'Rana'
            ]
        );
    }
}
